from SettingConfig import app
from Router.users.router import userRouter
from AllMiddlewere.ErrorMiddlewere.ErrorMiddlewere import ErrorMiddleware

app.wsgi_app = ErrorMiddleware(app.wsgi_app)
app.register_blueprint(userRouter)


def start():
    app.run('0.0.0.0', 5000)