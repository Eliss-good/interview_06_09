from pydantic import BaseModel, EmailStr, ConfigDict
from datetime import datetime


class SchemaInputUser(BaseModel):
    email: EmailStr
    username: str


class SchemaOutputUser(SchemaInputUser):
    id: int
    registration_date: datetime

    model_config = ConfigDict(from_attributes=True)
