from SettingConfig import settingConfig
from .schema import SchemaInputUser

from sqlalchemy import Column, String, DateTime, INTEGER
from datetime import datetime, timezone


def datetimeToInt():
    return int(datetime.now(tz=timezone.utc).timestamp())


class Users(settingConfig.Base):
    __tablename__ = "user"

    id = Column(INTEGER, primary_key=True, autoincrement=True)
    username = Column(String, unique=True, nullable=True)
    email = Column(String, unique=True, nullable=True)
    registration_date = Column(INTEGER, default=datetimeToInt, nullable=True)

    def __init__(self, dataObj: SchemaInputUser):
        self.__dict__.update(dataObj.model_dump())
