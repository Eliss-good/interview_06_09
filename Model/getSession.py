from functools import wraps
from SettingConfig import settingConfig

from typing import Callable
from flask import jsonify


def getSession(callbackError: Callable[[Exception], None] | None = None):
    """ Декоратор который создаёт сессию и передаёт её в функцию """
    def wrapper(func):
        @wraps(func)
        async def wrapped(*args, **kwargs):
            async with settingConfig.async_session() as session:
                kwargs["session"] = session

                try:
                    dataAnswer = await func(*args, **kwargs)
                    await session.commit()
                    return dataAnswer
                except Exception as e:
                    await session.rollback()
                    callbackError and callbackError(e)
                    raise e
        return wrapped

    return wrapper
