import os
from apiflask import APIFlask
from flask_cors import CORS
from dotenv import load_dotenv
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.ext.asyncio import create_async_engine
from sqlalchemy.orm import sessionmaker


load_dotenv('.env')


"""  Инцилизация Flask  """

app = APIFlask(__name__)
CORS(app)


"""  Конфигурация   """


class SettingConfig:
    def __intEnvToBool(self, envConfigParams: str):
        return bool(int(envConfigParams))

    def __init__(self):

        app.debug = self.__intEnvToBool(os.getenv('FLASK_DEBUG'))
        app.config['CORS_HEADERS'] = os.getenv("CORS_HEADERS")
        app.config['JSON_AS_ASCII'] = self.__intEnvToBool(os.getenv('JSON_AS_ASCII'))
        app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = self.__intEnvToBool(os.getenv('SQLALCHEMY_TRACK_MODIFICATIONS'))
        app.config['PROPAGATE_EXCEPTIONS'] = True

        self.dbConnect = os.getenv('DB_CONNECT')
        self.engine = create_async_engine(self.dbConnect, echo=True)
        self.Base = declarative_base()

        self.async_session = sessionmaker(self.engine, class_=AsyncSession)


settingConfig = SettingConfig()

