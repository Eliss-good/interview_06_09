from typing import Any
from enum import Enum
from datetime import datetime, timezone
import json


class TypesError(Enum):
    ServerError = "ServerError"
    UserError = "UserError"


class CustomError(Exception):
    code: int
    message: Any
    typeError: TypesError
    timeCreateError: datetime

    formattingMassage: dict

    def __init__(self,
                 message: Any = 'error',
                 code: int = 400,
                 typeError: TypesError = TypesError.ServerError):
        self.code = code
        self.message = message
        self.typeError = typeError
        self.timeCreateError = datetime.now(tz=timezone.utc)

        super().__init__(message if message else "Error")

    def createMessage(self):
        self.formattingMassage = {
            "typeError": self.typeError.value,
            "timeCreateError": self.timeCreateError.isoformat(),
            "errorMessage": self.message
        }

        return self.formattingMassage
