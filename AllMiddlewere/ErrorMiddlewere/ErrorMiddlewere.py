from werkzeug.wrappers import Response
from AllMiddlewere.ErrorMiddlewere.CustomError import CustomError
import json


class ErrorMiddleware():
    def __init__(self, app):
        self.app = app

    def __call__(self, environ, startResponse):
        try:
            return self.app(environ, startResponse)
        except Exception as e:
            e = CustomError( e.args[0] if type(e.args) == tuple and len(e.args) > 0 else str(e)) if type(e) != \
                    CustomError else e
            return Response(str(json.dumps(e.createMessage())), mimetype='application/json', status=e.code)(environ,
                                                                                                  startResponse)
