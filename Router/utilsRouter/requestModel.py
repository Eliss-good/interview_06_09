from functools import wraps
# from openapi_schema_pydantic.util import PydanticSchema
from pydantic import TypeAdapter, ValidationError
from flask import request


def requestModel(router, SchemaApiValidate, ModelSchemaPydantic):
    """  Декоратор который получает данные из Body в HTTP запросе и создаёт объект схемы Pydantic и передаёт это в
    endpoint """
    def wrapper(func):
        @wraps(func)
        @router.input(SchemaApiValidate)
        async def wrapped(*args, **kwargs):
            try:
                kwargs['json_data'] = ModelSchemaPydantic(**kwargs.get('json_data'))
            except ValidationError as e:
                return e.errors(), 422
            return await func(*args, **kwargs)
        return wrapped
    return wrapper
