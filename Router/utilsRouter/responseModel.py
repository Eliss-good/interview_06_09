from functools import wraps
from pydantic import TypeAdapter
import json


def responseModel(ModelType):
    """ Декоратор который сериализирует объект базы ORM """
    def wrapper(func):
        @wraps(func)
        async def wrapped(*args, **kwargs):
            data = TypeAdapter(ModelType).validate_python(await func(*args, **kwargs))
            return list(map(lambda x: json.loads(x.model_dump_json()), data)) if type(data) == list else \
                json.loads(data.model_dump_json())
        return wrapped
    return wrapper
