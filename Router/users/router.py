from Router.users.handlerRouter.handlerUserCrud import handlerGetUser, handlerAddUser, handlerDeleteUser,  \
    handlerUpdateUser, handlerPaginate
from Router.users.handlerRouter.handlerStructTask import handlerLastDateUser, handlerMaxUsername, handlerFindEmail
from Model.AllModels.User.schema import SchemaInputUser, SchemaOutputUser
from Router.utilsRouter.requestModel import requestModel
from Router.users.schemasRouter import AddUserSchemas

from flask import jsonify
from apiflask import APIBlueprint
from typing import List

userRouter = APIBlueprint('users', __name__)


@userRouter.get('/')
async def routerGetUser() -> List[SchemaOutputUser]:
    """ endpoint всех пользователей """
    return await handlerGetUser()


@userRouter.post('/')
@requestModel(userRouter, AddUserSchemas, SchemaInputUser)
async def routerAddUser(json_data: SchemaInputUser) :
    """ endpoint добавление нового пользователя """

    await handlerAddUser(json_data)
    return jsonify(None)


@userRouter.delete('/<int:userId>')
async def routerDeleteUser(userId: int):
    """ endpoint удаление пользователя """
    await handlerDeleteUser(userId)
    return jsonify(None)


@userRouter.put('/<int:userId>')
@requestModel(userRouter, AddUserSchemas, SchemaInputUser)
async def routerUpdateUser(userId: int, json_data: SchemaInputUser):
    """ endpoint обновление данных о пользователе """
    await handlerUpdateUser(userId=userId, data=json_data)
    return jsonify(None)


@userRouter.get('/lastDate')
async def routerLastDateUser() -> List[SchemaOutputUser]:
    """ endpoint выводит всех пользователей за последние 7 дней """
    return await handlerLastDateUser()


@userRouter.get('/maxLenUsername')
async def routerMaxNameUserName() -> List[SchemaOutputUser]:
    """ endpoint выводит 5 пользователей с самым длинным именем """
    return await handlerMaxUsername()


@userRouter.get('/findDomainEmail/<string:domainEmail>')
async def routerFindDomainEmail(domainEmail: str) -> str:
    """ endpoint выводит долю пользователей с определённым почтовым доменом """
    return await handlerFindEmail(domainEmail)


@userRouter.get('/paginate/<int:pageNumber>&<int:sizePage>')
async def routerPaginate(pageNumber: int, sizePage: int) -> List[SchemaOutputUser]:
    """ endpoint пагинации """
    return await handlerPaginate(pageNumber=pageNumber, sizePage=sizePage)



