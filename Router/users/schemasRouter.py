from apiflask import Schema
from apiflask.fields import String, Integer, DateTime


class AddUserSchemas(Schema):
    username = String(required=True)
    email = String(required=True)


class PaginateSchema(Schema):
    fromPage = Integer(required=False)
    toPage = Integer(required=False)


class UserOutPutApiSchema(Schema):
    username = String(required=True)
    email = String(required=True)
    id = Integer(required=True)
    registration_date = DateTime(required=True)
