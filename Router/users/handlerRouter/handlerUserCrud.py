from Model.AllModels.User.model import Users, SchemaInputUser
from Model.AllModels.User.schema import SchemaOutputUser
from Model.getSession import getSession
from AllMiddlewere.ErrorMiddlewere.CustomError import CustomError, TypesError
from Router.utilsRouter.responseModel import responseModel
from pydantic import BaseModel
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.exc import IntegrityError
from sqlalchemy import select, delete, update
from sqlalchemy import func

from typing import List


@getSession()
@responseModel(List[SchemaOutputUser])
async def handlerGetUser(session: AsyncSession):
    """  Обработчик выдаёт список всех пользователей  """

    allUsers = await session.execute(select(Users))
    return allUsers.scalars().all()


def callbackErrorDublicate(err: Exception):
    """ Функция которая сработает если в базе есть дубликат пользователя """
    if type(err) == IntegrityError:
        raise CustomError('Скорее всего такая почта уже существует', 400, TypesError.UserError)

    raise err


@getSession(callbackErrorDublicate)
async def handlerAddUser(data: SchemaInputUser, session: AsyncSession):
    """ Добавление новго пользователя """
    session.add(Users(data))


@getSession()
async def handlerDeleteUser(userId: int, session: AsyncSession):
    """ Обработчик удаления пользователя """
    data = await session.execute(delete(Users).where(Users.id == userId))
    if data.rowcount == 0:
        raise CustomError('Пользователя с таким id не существует', 400, TypesError.UserError)


@getSession(callbackErrorDublicate)
async def handlerUpdateUser(userId: int, data: SchemaInputUser, session: AsyncSession):
    """ Обработчик обновления данных пользователя """
    data = await session.execute(update(Users).where(Users.id == userId).values(data.model_dump()))
    if data.rowcount == 0:
        raise CustomError('Пользователя с таким id не существует', 400, TypesError.UserError)


@getSession()
@responseModel(List[SchemaOutputUser])
async def handlerPaginate(session: AsyncSession, pageNumber: int = 0, sizePage: int = 0):
    """  Обработчик пагинации """
    queryAllUser = await session.stream(select(Users).execution_options(yield_per=sizePage))

    dataPage: List[Users] = []
    while pageNumber > 0:
        try:
            dataPage = await queryAllUser.scalars().partitions().__anext__()
        except StopAsyncIteration:
            raise CustomError('Такой страницы не существует', 400, TypesError.UserError)

        pageNumber -= 1

    return dataPage
