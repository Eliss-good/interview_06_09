from Model.AllModels.User.model import Users
from Model.AllModels.User.schema import SchemaOutputUser
from Model.getSession import getSession
from Router.utilsRouter.responseModel import responseModel
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy import select
from datetime import datetime, timezone, timedelta
from sqlalchemy import func


from typing import List


@getSession()
@responseModel(List[SchemaOutputUser])
async def handlerLastDateUser(session: AsyncSession):
    """  Обработчик который выдаёт всех пользователей которые зарегистрировались последние 7 дней  """
    allUsers = await session.execute(select(Users).where(Users.registration_date >= (datetime.now(tz=timezone.utc) -
                                                         timedelta(days=7)).timestamp() ))
    return allUsers.scalars().all()


@getSession()
@responseModel(List[SchemaOutputUser])
async def handlerMaxUsername(session: AsyncSession):
    """  Обработчик который выдаёт пвсех пользователей с максимальной длинной в имени """
    lengthNameUser = func.char_length(Users.username).label("test")
    allUsers = await session.execute(select(Users, lengthNameUser).order_by(lengthNameUser.desc()).limit(5))
    return allUsers.scalars().all()


@getSession()
async def handlerFindEmail(domainEmail: str, session: AsyncSession):
    """  Обработчик который вычисляет долю определённого почтового домена """
    findDomainUser = await session.execute(select(func.count("*")).
                                           where(Users.email.like("%{}".format(f"@{domainEmail}"))))

    findAllUser = await session.execute(select(func.count("*")).select_from(Users))
    return f"{findDomainUser.scalar_one() / findAllUser.scalar_one() * 100}%"
