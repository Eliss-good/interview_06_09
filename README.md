### Для запуска проетка потребуется установить poetry

#### Шаги по запуску
1. [x] Клонируем репозиторий
2. [x] Заходим в папаку проекта
3. [x] Открывем консоль и пишем poetry install --only main && [poetry run start](````)

Проект запускается на хосте в 5000 порте.
Доступ к документации можно получить по адресу: http://localhost:5000/docs


### Архитектура папок:
 * [alembic.ini](./alembic.ini)
 * [migrations](./migrations)
   * [README](./migrations/README)
   * [script.py.mako](./migrations/script.py.mako)
   * [env.py](./migrations/env.py)
   * [versions](./migrations/versions)
 * [Router](./Router)
   * [utilsRouter](./Router/utilsRouter)
     * [requestModel.py](./Router/utilsRouter/requestModel.py)
     * [responseModel.py](./Router/utilsRouter/responseModel.py)
   * [users](./Router/users)
   * [schemasRouter.py](./Router/users/schemasRouter.py)
   * [router.py](./Router/users/router.py)
   * [handlerRouter](./Router/users/handlerRouter) - содержит все обработчики для маршрутов
     * [handlerStructTask.py](./Router/users/handlerRouter/handlerStructTask.py) - содержит обработчики для 2ого задания
     * [handlerUserCrud.py](./Router/users/handlerRouter/handlerUserCrud.py) - содержит обработчики для 1ого задания
 * [poetry.lock](./poetry.lock)
 * [AllMiddlewere](./AllMiddlewere)
   * [ErrorMiddlewere](./AllMiddlewere/ErrorMiddlewere) - middlewere для глобальной отловли ошибок и стандартизации
     * [CustomError.py](./AllMiddlewere/ErrorMiddlewere/CustomError.py) - кастомный тип ошибки
     * [ErrorMiddlewere.py](./AllMiddlewere/ErrorMiddlewere/ErrorMiddlewere.py) -  сам middlewere
 * [Model](./Model)
   * [AllModels](./Model/AllModels)
     * [User](./Model/AllModels/User) - ORM модель таблицы User
       * [model.py](./Model/AllModels/User/model.py)
       * [schema.py](./Model/AllModels/User/schema.py) - входная и выходная схема таблицы
   * [getSession.py](./Model/getSession.py)
 * [task.db](./task.db) - база данных
 * [pyproject.toml](./pyproject.toml)
 * [main.py](./main.py) - тут происходит запуск проекта
 * [README.md](./README.md)
 * [SettingConfig.py](./SettingConfig.py) - создаёт глобальный конфиг

